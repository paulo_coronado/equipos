package com.example.equipos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();

        String equipo = intent.getStringExtra("equipo");

        equipo.replaceAll(" ", "_");

        webView=findViewById(R.id.webView);

        String fullURL ="https://es.wikipedia.org/wiki/" + equipo;

        webView.setWebViewClient(new WebViewClient());

        webView.loadUrl(fullURL);


    }
}